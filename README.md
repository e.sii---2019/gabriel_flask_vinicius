# Instalação


1.  Clonar este repositório para o seu computador
2.  [Instalar o Python](https://www.python.org/)
3.  Por meio do pip instalar o Flask (no prompt de comando) <br>`$ pip install Flask`
4.  Instalar as outras dependencias do projeto <br> `$ pip install flask_sqlalchemy`<br> `$ pip install flask_marshmallow` <br> `$ pip install marshmallow-sqlalchemy`
5.  Abrir a pasta do projeto no terminal e executar o comando <br>`set FLASK_APP=app.py`
6.  executar o app <br> `python app.py`<br>
[vídeo](https://www.youtube.com/watch?v=C1QsgBQitmk)


# Utilização da API

1.  Quando o aplicativo estiver rodando, poderá ser acessado por localhost:5000
2.  Ele permite o cadastro de um produto por meio de um mensagem POST para a rota localhost:5000/produto como por exemplo: <br> `{
	"descricao" : "manteiga",
	"quantidade" : 3,
	"preco" : 4.00
}`
3. São permitidas as seguintes ações (para a rota /produto):
*  GET
*  GET(ID)
*  POST
*  PUT(ID)
*  DELETE(ID)
4. Ainda é possível calcular o total em dinheiro dos produtos pela rota /total