from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'produtos.sqlite')
#vincula SQLALchemy e Marshmallow ao nosso app
db = SQLAlchemy(app)
ma = Marshmallow(app)

# classe modelo de Produto
class Produto(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.String(80))
    quantidade = db.Column(db.Integer)
    preco = db.Column(db.Float)
    
    #construtor da classe
    def __init__(self, descricao, quantidade, preco):
        self.descricao = descricao
        self.quantidade = quantidade
        self.preco = preco

#classe que faz serializacao/deserializacao
class ProdutoSchema(ma.Schema):
    class Meta:
        #propriedades para serializacao/deserializacao Json
        fields = ('descricao', 'quantidade', 'preco')


produto_schema = ProdutoSchema()
produtos_schema = ProdutoSchema(many=True)


# rota para novo produto
@app.route("/produto/", methods=["POST"])
def add_produto():
    descricao = request.json['descricao']
    quantidade = request.json['quantidade']
    preco = request.json['preco']
  
    new_produto = Produto(descricao, quantidade, preco)

    db.session.add(new_produto)
    db.session.commit()

    return produto_schema.jsonify(new_produto)


# rota para mostrar todos os produtos
@app.route("/produto/", methods=["GET"])
def get_produtos():
    all_produtos = Produto.query.all()
    result = produtos_schema.dump(all_produtos)
    
    return jsonify(result.data)


# rota para mostrar total em reais
@app.route("/total/", methods=["GET"])
def soma_produtos():
    all_produtos = Produto.query.all()
    soma = 0
    for produto in all_produtos:
        soma = soma + (produto.preco * produto.quantidade)
    return "Soma dos produtos R$ : " + str(soma)


# rota para mostrar um produto pelo id
@app.route("/produto/<id_prod>", methods=["GET"])
def produto_detail(id_prod):
    produto = Produto.query.get(id_prod)
    
    return produto_schema.jsonify(produto)


# rota para modificacao de um produto
@app.route("/produto/<id_prod>", methods=["PUT"])
def produto_update(id_prod):
    produto = Produto.query.get(id_prod)
    descricao = request.json['descricao']
    quantidade = request.json['quantidade']
    preco = request.json['preco']
  

    produto.descricao = descricao
    produto.quantidade = quantidade
    produto.preco = preco
  
    db.session.commit()
    
    return produto_schema.jsonify(user)

# rota para deletar um produto
@app.route("/produto/<id_prod>", methods=["DELETE"])
def produto_delete(id_prod):
    produto = Produto.query.get(id_prod)
    db.session.delete(produto)
    db.session.commit()

    return produto_schema.jsonify(produto)


if __name__ == '__main__':
    app.run(debug=True)